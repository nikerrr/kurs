from django.contrib import admin

# Register your models here.

from .models import News


class NewsModelAdmin(admin.ModelAdmin):
    list_display = ["id", "title", "updated", "timestamp"]
    list_display_links = ["updated"]
    list_editable = ["title"]
    list_filter = ["updated", "timestamp"]
    search_fields = ["title", "content"]
    prepopulated_fields = {'slug': ('title',)}

    class Meta:
        model = News


admin.site.register(News, NewsModelAdmin)
