from django.urls import path, re_path
from . import views

app_name = 'news'


urlpatterns = [
    path('', views.news_list, name='list'),
    path('create/', views.news_create),
    re_path(r'^(?P<slug>[\w-]+)/$', views.news_detail, name='detail'),
    re_path(r'^(?P<slug>[\w-]+)/edit/$', views.news_update, name='update'),
    re_path(r'^(?P<slug>[\w-]+)/delete/$', views.news_delete, name='delete'),
]
