from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages

from .models import News
from .forms import NewsForm

# Create your views here.


def news_create(request):
    form = NewsForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "Created")
        return redirect(instance.get_absolute_url())
    context = {
        "form": form
    }
    return render(request, 'news_form.html', context)


def news_detail(request, slug=None):
    instance = get_object_or_404(News, slug=slug)
    context = {
        "title": instance.title,
        "instance": instance
    }
    return render(request, 'news_detail.html', context)


def news_list(request):
    queryset_list = News.objects.all()
    paginator = Paginator(queryset_list, 8)
    page_request_var = 'page'
    page = request.GET.get(page_request_var)
    queryset = paginator.get_page(page)
    context = {
        "object_list": queryset,
        "title": "Lab Explorer",
        "page_request_var": page_request_var
    }
    return render(request, 'news_list.html', context)


def news_update(request, slug=None):
    instance = get_object_or_404(News, slug=slug)
    form = NewsForm(request.POST or None, request.FILES or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "Saved")
        return redirect(instance.get_absolute_url())
    context = {
        "form": form
    }
    return render(request, 'news_form.html', context)


def news_delete(request, slug=None):
    instance = get_object_or_404(News, slug=slug)
    instance.delete()
    messages.success(request, "Successfully deleted")
    return redirect('news:list')
