# Generated by Django 2.0.4 on 2018-06-24 17:42

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Lab',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', models.SlugField(unique=True)),
                ('name', models.CharField(max_length=64)),
                ('country', models.CharField(max_length=64)),
                ('category', models.CharField(max_length=64)),
                ('subcategory', models.CharField(max_length=64)),
                ('language', models.CharField(max_length=64)),
                ('subject_domain', models.CharField(max_length=64)),
                ('equipment', models.CharField(max_length=64)),
                ('reusable_components', models.BooleanField()),
                ('lab_availability', models.CharField(max_length=64)),
            ],
        ),
    ]
