from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages

from .models import Lab
from .forms import LabForm

# Create your views here.

def lab_create(request):
    form = LabForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "Created")
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "form": form
    }
    return render(request, 'lab_form.html', context)


def lab_detail(request, slug=None):
    instance = get_object_or_404(Lab, slug=slug)
    context = {
        "title": instance.title,
        "instance": instance
    }
    return render(request, 'lab_detail.html', context)


def lab_list(request):
    queryset_list = Lab.objects.all()
    labs_per_page = 12
    paginator = Paginator(queryset_list, labs_per_page)
    page_request_var = 'page'
    page = request.GET.get(page_request_var)
    queryset = paginator.get_page(page)
    context = {
        "object_list": queryset,
        "title": "Labs",
        "page_request_var": page_request_var
    }
    return render(request, 'lab_list.html', context)


def lab_update(request, slug=None):
    instance = get_object_or_404(Lab, slug=slug)
    form = LabForm(request.POST or None, request.FILES or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "Saved")
        return redirect(instance.get_absolute_url())
    context = {
        "form": form
    }
    return render(request, 'lab_form.html', context)


def lab_delete(request, slug=None):
    instance = get_object_or_404(Lab, slug=slug)
    instance.delete()
    messages.success(request, "Successfully deleted")
    return redirect('labs:list')
