from django.urls import path, re_path
from . import views

app_name = 'labs'


urlpatterns = [
    path('', views.lab_list, name='list'),
    path('create/', views.lab_create),
    re_path(r'^(?P<slug>[\w-]+)/$', views.lab_detail, name='detail'),
    re_path(r'^(?P<slug>[\w-]+)/edit/$', views.lab_update, name='update'),
    re_path(r'^(?P<slug>[\w-]+)/delete/$', views.lab_delete, name='delete'),
]