from django.db import models
from django.urls import reverse
from django.db.models.signals import pre_save

import utils.functions as utils

# Create your models here.


class Lab(models.Model):
    slug = models.SlugField(unique=True)
    title = models.CharField(max_length=64)
    country = models.CharField(max_length=64)
    category = models.CharField(max_length=64)
    subcategory = models.CharField(max_length=64)
    language = models.CharField(max_length=64)
    subject_domain = models.CharField(max_length=64)
    equipment = models.CharField(max_length=64)
    reusable_components = models.BooleanField()
    lab_availability = models.CharField(max_length=64)

    def __unicode__(self):
        return self.title
       
    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('labs:detail', kwargs={"slug": self.slug})

pre_save.connect(utils.pre_save_receiver, sender=Lab)
