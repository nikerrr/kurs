from django.contrib import admin

# Register your models here.

from .models import Lab

class LabModelAdmin(admin.ModelAdmin):
    list_display = ["title"]
    class Meta:
        model=Lab

admin.site.register(Lab, LabModelAdmin)