from django import forms
from .models import Lab

class LabForm(forms.ModelForm):
    class Meta:
        model = Lab
        fields = [
            "title",
            "country",
            "category",
            "subcategory",
            "language",
            "subject_domain",
            "equipment",
            "reusable_components",
            "lab_availability"
        ]