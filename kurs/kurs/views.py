from django.shortcuts import render


def index_show(request):
    context = {
        "title": "Lab Explorer",
    }
    return render(request, 'index.html', context)

def about_show(request):
    context = {
        "title": "Lab Explorer",
    }
    return render(request, 'about.html', context)
