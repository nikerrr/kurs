from django.utils.text import slugify


def create_slug(sender, instance, slug_arg=None, slug_counter=1):
    if slug_arg is None:
        slug_arg = slugify(instance.title)
        slug = slug_arg
    else:
        slug = "{}-{}".format(slug_arg, slug_counter)
    qs = sender.objects.filter(slug=slug)
    if qs.exists():
        return create_slug(sender, instance, slug_arg, slug_counter + 1)
    return slug


def pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_slug(sender, instance)
